//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Foundation


class ___FILEBASENAME___DataManager {
    
    // MARK: Properties
    
    var interactor: ___FILEBASENAME___DataManagerInteractorProtocol!
}

extension ___FILEBASENAME___DataManager: ___FILEBASENAME___DataManagerProtocol {
    // TODO: Declare dataManager methods
}
