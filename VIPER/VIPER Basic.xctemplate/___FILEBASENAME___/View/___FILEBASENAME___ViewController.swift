//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Foundation
import UIKit


class ___FILEBASENAME___ViewController: UIViewController {
    
    // MARK: @IBOutlets
    
    // MARK: Properties

    var presenter: ___FILEBASENAME___ViewPresenterProtocol!

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: @IBActions
    
}

extension ___FILEBASENAME___ViewController: ___FILEBASENAME___ViewProtocol {
    // TODO: Declare view methods
}

