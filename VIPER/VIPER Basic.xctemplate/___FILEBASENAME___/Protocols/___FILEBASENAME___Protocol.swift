//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Foundation


protocol ___FILEBASENAME___ViewProtocol {
    // TODO: Declare view methods
}

protocol ___FILEBASENAME___ViewPresenterProtocol {
    // TODO: Declare presenter methods are used by view
}

protocol ___FILEBASENAME___InteractorPresenterProtocol {
    // TODO: Declare presenter methods are used by interactor
}

protocol ___FILEBASENAME___PresenterInteractorProtocol {
    // TODO: Declare interactor methods are used by presenter
}

protocol ___FILEBASENAME___WireframeProtocol {
    // TODO: Declare wireframe methods
}
