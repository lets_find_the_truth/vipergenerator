//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Foundation


class ___FILEBASENAME___Presenter {
    
    // MARK: Properties

    var view: ___FILEBASENAME___ViewProtocol!
    var wireframe: ___FILEBASENAME___WireframeProtocol!
    var interactor: ___FILEBASENAME___PresenterInteractorProtocol!
}

extension ___FILEBASENAME___Presenter: ___FILEBASENAME___ViewPresenterProtocol {
    // TODO: Declare presenter methods that are used by view
}

extension ___FILEBASENAME___Presenter: ___FILEBASENAME___InteractorPresenterProtocol {
    // TODO: Declare presenter methods that are used by interactor
}
