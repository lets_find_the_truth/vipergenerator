# ViperTemplate
Template to generate VIPER classes on Xcode.

## Installation
- [Download VIPER Template](https://gitlab.com/kyryl.nevedrov/viper-generator-nk/repository/archive.zip?ref=master) or clone the project
- Copy the `Design Pattern` folder to `~/Library/Developer/Xcode/Templates/File Templates/` or create a symbolic link to that folder.

## Using the template
- Start Xcode
- Create a new file (`File > New > File` or `⌘N`)
- Choose `VIPER` then either `VIPER basic` for basic VIPER structure or ‘VIPER standart’ for VIPER with data manager 

## Created Files
- FolderWithModuleName
	- `Protocols`
	- `Interactor`
	- `Presenter`
	- `View` 
	- `Wireframe`
	- ‘DataManager’ (only if ‘VIPER standart’)
