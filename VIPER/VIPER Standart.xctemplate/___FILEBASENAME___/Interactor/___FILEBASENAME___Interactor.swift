//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Foundation


class ___FILEBASENAME___Interactor {
    
    // MARK: Properties
    
    var presenter: ___FILEBASENAME___InteractorPresenterProtocol!
    var dataManager: ___FILEBASENAME___DataManagerProtocol!
}

extension ___FILEBASENAME___Interactor: ___FILEBASENAME___PresenterInteractorProtocol {
    // TODO: Declare interactor methods are used by presenter
}

extension ___FILEBASENAME___Interactor: ___FILEBASENAME___DataManagerInteractorProtocol {
    // TODO: Declare interactor methods are used by dataManager
}
